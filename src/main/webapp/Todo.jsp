<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.sda.jsp.keys.Keys" %>

<%--
  Lista zakupów trzymana w sesji - bedzie unikalna dla kazdego uzytkownika
  Jesli dodamy kolejna pozycje zostanie ona dodana do listy
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Jesli wchodzimy na strone pierwszy raz lista zakupow nie istniaje --%>
<%
    List<String> zakupy = (List<String>) session.getAttribute(Keys.LISTA_ZAKUPOW);
    if(zakupy == null){
        zakupy = new ArrayList<String>();
        session.setAttribute(Keys.LISTA_ZAKUPOW,zakupy);
    }

%>

<html>
<head>
    <title>Title</title>
</head>
<body>
<form action ="Todo.jsp">
    <input name="Item" type="text">
    <input type="submit" value="Approve">
</form>
<%  String element=request.getParameter("Item");
    if(element!=null)
        zakupy.add(element);
    for (String zakup:zakupy) {
        out.println(zakup+"<br>");
    }%>
</body>
</html>

<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.Enumeration" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Metody które są w deklaracji (tag <%! %> sa dostepne w całej stronie JSP--%>
<%!
    public String toUpperCase(String a) {
        return a.toUpperCase();
    }
%>


<html>
<head>
    <%-- Dodajemy do treści trony plik ze stylami bootstrapa --%>
    <%@include file="bootstrap.jsp" %>
    <title>$Title$</title>
</head>
<body>

<div class="container" style="margin-top: 20px">

    <%-- Aby wyswietlic tresc na stronie w skryptlecie musimy uzyc zmiennej wbudowanej out
        Przy okazji widzimy użycie metody toUpperCase która zdefiniowalismy wyzej w bloku deklaracji
        --%>
    <%
        String b = toUpperCase("Helloł");
        out.print(b);
    %>
    <%-- TODO Hello World i aktualna godzina--%>

        <%-- We wyrażaniech nie musimy pisać out.print(). Wszystko jest wypisane jako treść strony--%>
    <%= new String("Hello World!")%>
    <br>
    <%= LocalDateTime.now()%>
    <br>
    <%= "Mogę od razu pisać String, kompilator jest mądry i wie, że nie trzeba robić new String()" %>
    <br>
    <%= "Moge też wstwić znacznik HTML, np. <h3> To jest nagłówek </h3>" %>

    Aktualna data to:
    <br/>
    25 razy 4 to:
    <%
        int result = 4 * 25;
        out.println(result);
        System.out.print(result);
    %>
    <br/>
    Czy 50 jest mniejsze od 21?
    <%
        boolean a = 50 < 21;
        out.print(a);
    %>
    <br/>
    <%-- TODO zmienne zdefiniowane --%>
    Przeglądarka użytkownika:
        <%-- Nagłówki ządanie pobieramy metodą getHeader('') --%>
    <%= request.getHeader("user-agent")%>
    <br/>
    Język zapytania:
        <%-- Język jaki jest ustawiony w przegladarce uzytkownika --%>
    <%= request.getLocale()%>
    <br/>
    Lista wszystkich nagłówków zapytania
    <%
        Enumeration<String> headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String headerName = headers.nextElement();
            out.println(headerName + ":" + request.getHeader(headerName) + "<br>");
        }
    %>
</div>


<%-- TODO Obsługa formy --%>
<%-- TODO Dodać <select> checkbox i radiobutton --%>
<%--
<div class="container" style="margin-top: 20px">
    <form>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder="Enter email" name="email">
            <small id="emailHelp" class="form-text text-muted">Nigdy nie udostepnimy Twojego e-maila</small>
        </div>
        <div class="form-group">
            <label for="imie">Imie</label>
            <input type="text" class="form-control" id="imie" placeholder="Imie">
        </div>
        <div class="form-group">
            <label for="nazwisko">Nazwisko</label>
            <input type="text" class="form-control" id="nazwisko" placeholder="Nazwisko">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
--%>
<%@include file="footer.jsp" %>
</body>
</html>

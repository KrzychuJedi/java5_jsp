<%@ page import="java.util.List" %>
<%@ page import="org.sda.jsp.keys.Keys" %><%--
  Created by IntelliJ IDEA.
  User: krzysztof.gonia
  Date: 1/19/2018
  Time: 6:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    List<String> listaZakupow = (List<String>) session.getAttribute(Keys.LISTA_ZAKUPOW);
    if (listaZakupow != null) {
        for (String zakup : listaZakupow) {
            out.println(zakup + "<br>");
        }
    }%>
</body>
</html>

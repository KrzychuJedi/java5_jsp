<%--
  Wyciągamy ciasteczka
  Kod wypisze nam klucze i wartosci wszystkch ciasteczek jakie są dostępne dla naszej domeny (np. localhost)
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<% Cookie [] cookieTab = request.getCookies();
    for (Cookie cookieTemp : cookieTab){
        out.println(cookieTemp.getName()+" : ");
        out.println(cookieTemp.getValue()+"<br>");
    };
%>
</body>
</html>
